@extends('dashboard.layouts.master')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>

@section('page-title', trans('app.permissions'))

@section('page-header')
    <h1>
        {{ $edit ? $page->page : "New Page" }}
      </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
        <li><a href="{{ route('dashboard.cms.index') }}">CMS</a></li>
        <li class="active">{{ $edit ? trans('app.edit') : trans('app.create') }}</li>
      </ol>
@endsection

@section('content')


@include('partials.messages')

    {!! Form::open(['route' => ['dashboard.cms.update', $page->id], 'method' => 'PUT', 'id' => 'permission-form']) !!}


<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">CMS Page</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="name">@lang('app.name')</label>
                    <input type="text" class="form-control" id="name"
                           name="page" readonly="readonly" placeholder="@lang('app.permission_name')" value="{{ $edit ? $page->page : old('name') }}">
                </div>
                <div class="form-group">
                    <label for="description">@lang('app.description')</label>
                    <textarea rows=15 name="description" id="description" class="form-control">{{ $edit ? $page->content : old('description') }}</textarea>
                </div>
                </div>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-md-2">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
              Update
        </button>
    </div>
</div>

@stop
