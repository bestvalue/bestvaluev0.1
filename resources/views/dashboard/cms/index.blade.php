@extends('dashboard.layouts.master')

@section('page-title', trans('app.permissions'))

@section('page-header')
    <h1>
      C M S Page
        <small>available CMS page list</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
        <li class="active"></li>
      </ol>
@endsection

@section('content')

@include('partials.messages')

<div class="box">
    <div class="box-header">
        <h3 class="box-title">CMS Page Lists</h3>
        <br><br>
      </div>
      <div class="box-body table-responsive no-padding" id="users-table-wrapper">
            <table class="table table-hover">
                <thead>
                    <th>#</th>
                    <th class="text-center">Page Name</th>
                    <th class="text-center">Page Updated On</th>
                    <th class="text-center">Edit</th>
                </thead>
                <tbody>
                    @if (count($pages))
                    @foreach($pages as $k=>$page)
                    <tr>
                        <td>{{ ++$k}}</td>
                        <td class="text-center">{{ $page->page }}</td>
                        <td class="text-center">{{ $page->updated_at }}</td>
                        <td class="text-center">
                            <a href="{{ route('dashboard.cms.edit', $page->id) }}" class="btn btn-primary btn-circle"
                                title="Edit Page" data-toggle="tooltip" data-placement="top">
                                <i class="glyphicon glyphicon-edit"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="4"><em>@lang('app.no_records_found')</em></td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
  </div>



@stop
