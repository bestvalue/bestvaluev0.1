@extends('dashboard.layouts.master')

@section('page-title', 'Add-Edit Job Opening')

@section('page-header')
    <h1>
        {{ $edit ? 'Edit Job' : 'Add New Job Opening' }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
        <li><a href="{{ route('dashboard.jobs.index') }}">Job Opening</a></li>
        <li class="active">{{ $edit ? trans('app.edit') : trans('app.create') }}</li>
      </ol>
@endsection

@section('content')


@include('partials.messages')

@if ($edit)
    {!! Form::open(['route' => ['dashboard.jobs.update', $opening->id], 'method' => 'PUT', 'id' => 'question-form']) !!}
@else
    {!! Form::open(['route' => 'dashboard.jobs.store', 'id' => 'Job-Opening-form']) !!}
@endif

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Job Openings Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="name">Job Title </label>
                    <input type="text" class="form-control" id="question"
                           name="title" placeholder="Job-title"  required="required" value="{{ $edit ? $opening->title : old('name') }}">
                </div>
                <div class="form-group">
                    <label for="display_name">Job Description</label>
                      <textarea rows=5 name="description"  required="required"  id="description" placeholder="description" class="form-control">{{ $edit ? $opening->description :'' }}</textarea>
                </div>
            </div>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-md-2">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
            {{ $edit ? 'Update' : 'Create'  }}
        </button>
    </div>
</div>

@stop
