@extends('dashboard.layouts.master')

@section('page-title', 'FAQ')

@section('page-header')
    <h1>
      F A Q's
        <small>Questions and Answers</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
        <li class="active">Faq page</li>
      </ol>
@endsection

@section('content')

@include('partials.messages')

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Available  FAQ</h3>
        <div class="box-tools">
            <a href="{{ route('dashboard.faq.create') }}" class="btn btn-sm btn-success">
                <i class="fa fa-plus"></i>
                Add New Question
            </a>
        </div>
    </div>
    <div class="box-body table-responsive no-padding" id="users-table-wrapper">
        <table class="table table-hover">
            <thead>
                <th>#</th>
                <th class="text-center">Question</th>
                <th class="text-center">Answer</th>
                <th class="text-center">@lang('app.action')</th>
            </thead>
            <tbody>
                @if (count($questions))
                @foreach ($questions as $k=>$question)
                <tr>
                    <td>{{ ++$k  }}</td>
                    <td>{{ $question->question  }}</td>
                    <td>{{ $question->answer  }}</td>
                    <td class="text-center">
                        <a href="{{ route('dashboard.faq.edit', $question->id) }}" class="btn btn-primary btn-circle"
                            title="edit" data-toggle="tooltip" data-placement="top">
                            <i class="glyphicon glyphicon-edit"></i>
                        </a>
                        <a href="{{ route('dashboard.faq.destroy', $question->id) }}" class="btn btn-danger btn-circle"
                            title="delete"
                            data-toggle="tooltip"
                            data-placement="top"
                            data-method="DELETE"
                            data-confirm-title="@lang('app.please_confirm')"
                            data-confirm-text="are you sure delete question"
                            data-confirm-delete="Yes">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="4"><em>@lang('app.no_records_found')</em></td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>


</div>

@section('after-scripts-end')
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_flat',
          radioClass: 'iradio_flat',
          increaseArea: '20%' // optional
        });
      });
    </script>
@stop

@stop
