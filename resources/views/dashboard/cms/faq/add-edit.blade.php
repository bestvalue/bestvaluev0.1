@extends('dashboard.layouts.master')

@section('page-title', 'Add-Edit FAQ')

@section('page-header')
    <h1>
        {{ $edit ? 'Edit Question' : 'Add New Question' }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
        <li><a href="{{ route('dashboard.faq.index') }}">FAQ</a></li>
        <li class="active">{{ $edit ? trans('app.edit') : trans('app.create') }}</li>
      </ol>
@endsection

@section('content')


@include('partials.messages')

@if ($edit)
    {!! Form::open(['route' => ['dashboard.faq.update', $question->id], 'method' => 'PUT', 'id' => 'question-form']) !!}
@else
    {!! Form::open(['route' => 'dashboard.faq.store', 'id' => 'question-form']) !!}
@endif

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Question details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="name">Question</label>
                    <input type="text" class="form-control" id="question"
                           name="question" placeholder="Question"  required="required" value="{{ $edit ? $question->question : old('name') }}">
                </div>
                <div class="form-group">
                    <label for="display_name">Answer</label>
                      <textarea rows=5 name="answer"  id="answer"  required="required" placeholder="answer" class="form-control">{{ $edit ? $question->answer :'' }}</textarea>
                </div>
            </div>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-md-2">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
            {{ $edit ? 'Update' : 'Create'  }}
        </button>
    </div>
</div>

@stop
