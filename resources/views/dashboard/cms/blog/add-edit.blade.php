@extends('dashboard.layouts.master')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>
@section('page-title', 'Add-Edit Blogs')

@section('page-header')
    <h1>
        {{ $edit ? 'Edit Job' : 'Add New Blog' }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
        <li><a href="{{ route('dashboard.blogs.index') }}">Blog</a></li>
        <li class="active">{{ $edit ? trans('app.edit') : trans('app.create') }}</li>
      </ol>
@endsection

@section('content')


@include('partials.messages')

@if ($edit)
    {!! Form::open(['route' => ['dashboard.blogs.update', $blog->id], 'method' => 'PUT', 'id' => 'Blog-Form','files'=>true]) !!}
@else
    {!! Form::open(['route' => 'dashboard.blogs.store', 'id' => 'BLog-form','files'=>true]) !!}
@endif

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Blog details </div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="name">Blog Title </label>
                    <input type="text" class="form-control" id="title"
                           name="title" required="required"  placeholder="Blog-title" value="{{ $edit ? $blog->title : old('title') }}">
                </div>
                <div class="form-group">
                    <label for="file">Image</label>
                    <input type="file" class="form-control" id="file" name="image" placeholder="Select your Image">
                    @if($edit and !empty($blog->image))
                    <br>
                      {!! Html::image('upload/blogs/'.$blog->image) !!}
                    @endif
                </div>
                <div class="form-group">
                    <label for="display_name">Content</label>
                      <textarea rows=5 name="Content" id="Content" placeholder="Content" class="form-control">{{ $edit ? $blog->contents :old('Content') }}</textarea>
                </div>
            </div>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-md-2">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save"></i>
            {{ $edit ? 'Update' : 'Create'  }}
        </button>
    </div>
</div>

@stop
