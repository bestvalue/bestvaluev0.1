@extends('dashboard.layouts.master')

@section('page-title', 'Blog Lists')

@section('page-header')
    <h1>
  Blogs Details
        <small> Manange Blogs </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('app.home')</a></li>
        <li class="active">Blog page</li>
      </ol>
@endsection

@section('content')

@include('partials.messages')

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Blog</h3>
        <div class="box-tools">
            <a href="{{ route('dashboard.blogs.create') }}" class="btn btn-sm btn-success">
                <i class="fa fa-plus"></i>
                Add New
            </a>
        </div>
    </div>
    <div class="box-body table-responsive no-padding" id="users-table-wrapper">

        <table class="table table-hover">
            <thead>
                <th>#</th>
                <th class="text-center">Title</th>
                <th class="text-center">Posted By</th>
                <th class="text-center">@lang('app.action')</th>
            </thead>
            <tbody>
                @if (count($blogs))
                @foreach ($blogs as $k=>$blog)
                <tr>
                    <td>{{ ++$k  }}</td>
                    <td>{{ $blog->title  }}</td>
                    <td>Admin</td>
                    <td class="text-center">
                        <a href="{{ route('dashboard.blogs.edit', $blog->id) }}" class="btn btn-primary btn-circle"
                            title="edit" data-toggle="tooltip" data-placement="top">
                            <i class="glyphicon glyphicon-edit"></i>
                        </a>
                        <a href="{{ route('dashboard.blogs.destroy', $blog->id) }}" class="btn btn-danger btn-circle"
                            title="delete"
                            data-toggle="tooltip"
                            data-placement="top"
                            data-method="DELETE"
                            data-confirm-title="@lang('app.please_confirm')"
                            data-confirm-text="are you sure delete Blog"
                            data-confirm-delete="Yes">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="4"><em>@lang('app.no_records_found')</em></td>
                </tr>
                @endif
              </tbody>
        </table>
    </div>


</div>



@stop
