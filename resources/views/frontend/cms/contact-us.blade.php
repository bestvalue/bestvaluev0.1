@extends('frontend.layouts.master')
@section('content')
<section>
    <div class="container-fluid no-padding">
        <div class="contactusbnr">
            <div class="container">
                <div class="pagetxt">Home / Contact Us</div>
               <div class="contactblk">
                    <h3>Contact Us</h3>
                   <div class="contactform">
                        <p>Our customer service team is always available to answer your question or receive your feedback.Please do get in touch with us anytime via the form below and we will endevour to get back to you as soon as possible</p>
                    <div>
                    {{ Form::open(array('route' => 'frontend.contactPost', 'method' => 'post')) }}
                        <div class="col-md-12 no-padding"><label>Select a Subject</label></div>
                        <div class="col-md-12 no-padding">
                            <select required="required">
                              <option value="cashBack">CashBack</option>
                              <option value="rewards">Rewards</option>
                              <option value="partner-with-us">Partner with us</option>
                              <option value="other">Other</option>
                            </select>
                    </div>
                       <div>
                        <div class="col-md-12 no-padding"><label>Name</label></div>
                        <div class="col-md-12 no-padding"><input type="text" name="name" required="required" placeholder="Type Your Name"/></div>
                       </div>
                        <div>
                        <div class="col-md-12 no-padding"><label>Email Address</label></div>
                        <div class="col-md-12 no-padding"><input type="email"  placeholder="ex@mail.com"/></div>
                       </div>
                        <div>
                       <div class="col-md-12 no-padding"> <label>Your Message</label></div>
                        <div class="col-md-12 no-padding"><textarea  placeholder="Type Your Message Here"></textarea></div>
                       </div>
                        <!-- <div class="capcha"><img src="images/capcha.png"/></div> -->
                        <div class="submitbtn"><button>SUBMIT</button></div>
                        {{ Form::close() }}
                   </div>
                </div>
                <div class="adblk">
                        <img class="img-responsive" src="{{ asset('assets/images/ad1.png')}}"/>
                        <div class="ofrbox">
                            <h5>Find your Best Offers From:</h5>
                            <div class="col-md-12 sites no-padding">
                                <ul>
                                    <li><img src="{{ asset('assets/images/flipkart.png')}}"/></li>
                                    <li><img src="{{ asset('assets/images/homeshp.png')}}"/></li>
                                    <li><img src="{{ asset('assets/images/freechrg.png')}}"/></li>
                                    <li><img src="{{ asset('assets/images/paytm.png')}}"/></li>
                                </ul>
                            </div>
                             <div class="col-md-12 sites no-padding">
                                <ul>
                                    <li><img src="{{ asset('assets/images/naptol.png')}}"/></li>
                                    <li><img src="{{ asset('assets/images/infi.png')}}"/></li>
                                    <li><img src="{{ asset('assets/images/shpclues.png')}}"/></li>
                                    <li><img src="{{ asset('assets/images/jabong.png')}}"/></li>
                                </ul>
                            </div>
                             <div class="col-md-12 sites no-padding">
                                <ul>
                                    <li><img src="{{ asset('assets/images/amazon.png')}}"/></li>
                                    <li><img src="{{ asset('assets/images/ebay.png')}}"/></li>
                                    <li><img src="{{ asset('assets/images/snapdeal.png')}}"/></li>
                                    <li><img src="{{ asset('assets/images/myntra.png')}}"/></li>
                                </ul>
                            </div>
                        </div>
                    <img class="ad img-responsive" src="{{ asset('assets/images/ad2.png')}}"/>
                    <img class="img-responsive" src="{{ asset('assets/images/ad3.png')}}"/>
                </div>
          </div>
        </div>
    </div>
</section>
<section class="container">
	<div class="newsletter abt">
    	<div class="col-md-5 col-sm-5 col-xs-12">
            <div class="buy">
            <h4>Buy and Earn</h4>
            <h1>Extra Cash Back</h1>
            </div>
            <img src="{{ asset('assets/images/hand.png') }}" />
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 log">
            <p>log on to</p>
            <h4>www.bestvalue.com</h4>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 btmlogo no-padding"><img src="{{ asset('assets/images/btmlogo.png') }}"/></div>
    </div>
</section>
<script>
			$(function() {
				var demo1 = $("#demo1").slippry({
					// transition: 'fade',
					// useCSS: true,
					// speed: 1000,
					// pause: 3000,
						auto: true,
					// preload: 'visible',
					// autoHover: false
				});

			});
		</script>
@endsection
