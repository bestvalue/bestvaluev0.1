@extends('frontend.layouts.master')
@section('content')
<div class="container-fluid inner_bg">
  <div class="container">
    <div class="row">
      <div class="col-md-12 space1">
        <p><a href="#">Home</a> / Blog</p>
        <h1>Blogs</h1>
      </div>
      <div class="col-md-12">
        <div class="inner-panel blog-panel clearfix">
          <div class="col-md-8">
            @if (count($blogs))
             @foreach ($blogs as $blog)
            <div class="blog-cnt">
              <div class="blog-img">@if(!empty($blog->image)) <img src="{{ asset('upload/blogs/'.$blog->image)}}" alt="">  @endif</div>
              <div class="headline clearfix">
                <div class="date">{{ date('d', strtotime($blog->posted_on)) }}<br>
                  {{ strtoupper(date('M', strtotime($blog->posted_on))) }} </div>
                <div class="headtitle">
                  <h2><a href="{{ URL::to('/blog/' . $blog->id) }}">{{ $blog->title }}</a></h2>
                  <div class="info-data">
                    <ul class="list-inline">
                      <li> <span><i class="fa fa-user"></i> By: Admin</span> </li>
                      <li> <span><i class="fa fa-comments"></i> Comments: 0</span> </li>
                    </ul>
                  </div>
                </div>
              </div>
              <p>{!! str_limit($blog->contents, $limit = 250, $end = '...') !!} </p>
            </div>
            @endforeach
          @endif
        </div>
          <div class="col-md-4">
            <div class="blog-lft">
              <input type="search" value="" placeholder="Enter Search keywords" class="srch">
              <button type="button" class="btn btn-default rch-btn"><i class="fa fa-search" aria-hidden="true"></i> </button>
              <aside class="blog-categories">
                <h3>Categories</h3>
                <ul class="list-unstyled">
                  <li><a href="#">Deals of products Network</a></li>
                  <li><a href="#">Warehouse Management</a></li>
                  <li><a href="#">Road Way Delivery</a></li>
                  <li><a href="#">Shipment of Products</a></li>
                  <li><a href="#">Warehouse and Goods</a></li>
                  <li><a href="#">Air Frieght</a></li>
                </ul>
              </aside>
              <aside class="popularpost">
                <h3>Popular Posts</h3>
                <article class="item-post clearfix"> <a class="post-img" href="#"> <img class="img-responsive" src="{{ asset('assets/images/popular-post01.jpg')}}" alt="">
                  <h6>Duisau irure dolor</h6>
                  </a>
                  <p>in reprehenderit in volup tate velit esse cillum <span>02 Dec </span></p>
                </article>
                <article class="item-post clearfix"> <a class="post-img" href="#"> <img class="img-responsive" src="{{ asset('assets/images/popular-post02.jpg') }}" alt="">
                  <h6>Duisau irure dolor</h6>
                  </a>
                  <p>in reprehenderit in volup tate velit esse cillum <span>02 Dec </span></p>
                </article>
                <article class="item-post clearfix"> <a class="post-img" href="#"> <img class="img-responsive" src="{{ asset('assets/images/popular-post03.jpg')}}" alt="">
                  <h6>Duisau irure dolor</h6>
                  </a>
                  <p>in reprehenderit in volup tate velit esse cillum <span>02 Dec </span></p>
                </article>
              </aside>
              <div class="gallery-image">
                <h3>Gallery</h3>
                <a href="#"><img alt="" src="{{ asset('assets/images/gallery01.jpg') }}"></a> <a href="#"><img alt="" src="{{ asset('assets/images/gallery02.jpg') }}"></a> <a href="#"><img alt="" src="{{ asset('assets/images/gallery03.jpg') }}"></a> <a href="#"><img alt="" src="{{ asset('assets/images/gallery04.jpg')}}"></a> <a href="#"><img alt="" src="{{ asset('assets/images/gallery05.jpg') }}"></a> <a href="#"><img alt="" src="{{ asset('assets/images/gallery06.jpg')}}"></a> </div>
            </div>
          </div>
        </div>
      </div>
      <div class="shadow"></div>
    </div>
  </div>
</div>
<script>
			$(function() {
				var demo1 = $("#demo1").slippry({
					// transition: 'fade',
					// useCSS: true,
					// speed: 1000,
					// pause: 3000,
						auto: true,
					// preload: 'visible',
					// autoHover: false
				});

			});
		</script>
@endsection
