@extends('frontend.layouts.master')
<style>
/* Remove the navbar's default margin-bottom and rounded borders */
.navbar {
	margin-bottom: 0;
	border-radius: 0;
}
/* Add a gray background color and some padding to the footer */
footer {
	background-color: #f2f2f2;
	padding: 25px;
}
</style>
@section('content')
<div class="container-fluid partner">
  <div class="container">
  <div class="row">
    <div class="col-md-12 space1">
      <p><a href="#">Home</a> / Partner</p>
    </div>
    </div>
		<div class="col-md-12">
			<div class="partner_with">
				<h1>Partner with us</h1>
				<h2>and enhance your business</h2>
		    {{ Form::open(array('route' => 'frontend.contactPost', 'method' => 'post')) }}
					<div><textarea placeholder="description of your business" required="required"></textarea><i class="fa fa-star" aria-hidden="true"></i></div>
					<div><input type="text" placeholder="Website URL/Address of your business"/><i class="fa fa-star" aria-hidden="true"></i></div>
					<div>
							<span class="col-md-6 col-sm-6 col-xs-6 no-padding"><input type="tel" required="required" placeholder="Phone Number"/></span>
							<span class="col-md-6 col-sm-6 col-xs-6 no-padding-right"><input type="email" required="required" placeholder="Enter Email Address"></span>
							<i class="fa fa-star" aria-hidden="true"></i>
					</div>
					<div><input type="text" placeholder="Where did you hear about us?"/></div>
				 <h4> Please enter you email address, we will get back to you.</h4>
				<div class="okbtn">
					<button class="btn btn-warning " type="submit">submit</button>
				</div>
				  {{ Form::close() }}
			</div>
		</div>
		<div class="col-md-12 ">
			<div class="what"> What Our Partners Say</div>
		</div>
	</div>
  <div class="row">
    <div class="bdr"></div>
    <div class="down_arrow"></div>
  </div>
 <div class="container">
  <ul class="bxslider">
          <li> <img src="{{ asset('assets/images/test-pic01.png') }}">
            <h2> Paul Filip</h2>
          <h3> CEO, IndiaShopping.com</h3>
            <p>Donec hendrerit accumsan urna, eget luctus justo malesuada at. Proin non venenatis eros, sed aliquet metus. Pellentesque habitant morbitristique senectus et netus et malesuada fames ac turpis egestas. Maecenas metus metus, varius quis mi non, vestibulum cursus augue. Aliquam quis vestibulum nibh. Morbi laoreet elit ut tincidunt eleifend. Nulla facilisi. Donec gravida erat quis odio accumsan mollis. </p>
          </li>
          <li> <img src="{{ asset('assets/images/test-pic01.png') }}">
            <h2> Paul Filip</h2>
          <h3> CEO, IndiaShopping.com</h3>
            <p>Donec hendrerit accumsan urna, eget luctus justo malesuada at. Proin non venenatis eros, sed aliquet metus. Pellentesque habitant morbitristique senectus et netus et malesuada fames ac turpis egestas. Maecenas metus metus, varius quis mi non, vestibulum cursus augue. Aliquam quis vestibulum nibh. Morbi laoreet elit ut tincidunt eleifend. Nulla facilisi. Donec gravida erat quis odio accumsan mollis. </p>
          </li>
          <li> <img src="{{ asset('assets/images/test-pic01.png') }}">
            <h2> Paul Filip</h2>
          <h3> CEO, IndiaShopping.com</h3>
            <p>Donec hendrerit accumsan urna, eget luctus justo malesuada at. Proin non venenatis eros, sed aliquet metus. Pellentesque habitant morbitristique senectus et netus et malesuada fames ac turpis egestas. Maecenas metus metus, varius quis mi non, vestibulum cursus augue. Aliquam quis vestibulum nibh. Morbi laoreet elit ut tincidunt eleifend. Nulla facilisi. Donec gravida erat quis odio accumsan mollis. </p>
          </li>
    </ul>
 </div>
 <section class="container">
 	<div class="newsletter abt">
     	<div class="col-md-5 col-sm-5 col-xs-12">
             <div class="buy">
             <h4>Buy and Earn</h4>
             <h1>Extra Cash Back</h1>
             </div>
             <img src="{{ asset('assets/images/hand.png') }}" />
         </div>
         <div class="col-md-5 col-sm-5 col-xs-12 log">
             <p>log on to</p>
             <h4>www.bestvalue.com</h4>
         </div>
         <div class="col-md-2 col-sm-2 col-xs-12 btmlogo no-padding"><img src="{{ asset('assets/images/btmlogo.png') }}"/></div>
     </div>
 </section>
</div>
<script>
$('.bxslider').bxSlider({
	infiniteLoop: false,
	hideControlOnEnd: true
});
</script>

@endsection
