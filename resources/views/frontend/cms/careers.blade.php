@extends('frontend.layouts.master')
@section('content')
<div class="container-fluid inner_bg">
  <div class="container">
    <div class="row">
      <div class="col-md-12 space1">
        <p><a href="#">Home</a> / Privacy Policy</p>
        <h1>Careers</h1>
      </div>
      <div class="col-md-12">
        <div class="inner-panel">
          <div class="careers clearfix">
          <img src="{{ asset('assets/images/careers.jpg') }}" alt="">
            <p>  {!! $page->content !!} </p>
            </div>

       	@if (count($jobs))
          @foreach ($jobs as $job)
            <div class="careers-list clearfix">
              <h2>{{ $job->title }}</h2>
              <p>
                {{ $job->description }}
              </p>
              <div class="resume">
                  	Email your resume to <a href="#">jobs@bestvalueperks.com</a>
              </div>
            </div>
              @endforeach
          </ul>
          @endif
        </div>
      </div>
      <div class="shadow"></div>
    </div>
  </div>
</div>
<script>
			$(function() {
				var demo1 = $("#demo1").slippry({
					// transition: 'fade',
					// useCSS: true,
					// speed: 1000,
					// pause: 3000,
						auto: true,
					// preload: 'visible',
					// autoHover: false
				});

			});
		</script>
@endsection
