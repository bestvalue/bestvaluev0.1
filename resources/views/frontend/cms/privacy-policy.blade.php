@extends('frontend.layouts.master')
@section('content')
<div class="container-fluid inner_bg">
  <div class="container">
    <div class="row">
      <div class="col-md-12 space1">
        <p><a href="#">Home</a> / Privacy Policy</p>
        <h1>Privacy Policy</h1>
      </div>
      <div class="col-md-12">
        <div class="privacy">
          {!! $page->content !!}
        </div>
      </div>
      <div class="shadow"></div>
    </div>
  </div>
</div>
<script>
			$(function() {
				var demo1 = $("#demo1").slippry({
					// transition: 'fade',
					// useCSS: true,
					// speed: 1000,
					// pause: 3000,
						auto: true,
					// preload: 'visible',
					// autoHover: false
				});

			});
		</script>
@endsection
