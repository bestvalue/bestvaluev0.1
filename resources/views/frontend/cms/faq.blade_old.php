@extends('frontend.layouts.master')
<style>
/* Remove the navbar's default margin-bottom and rounded borders */
.navbar {
	margin-bottom: 0;
	border-radius: 0;
}
/* Add a gray background color and some padding to the footer */
footer {
	background-color: #f2f2f2;
	padding: 25px;
}
</style>
@section('content')
<div class="container-fluid inner_bg">
  <div class="container">
    <div class="row">
      <div class="col-md-12 space1">
        <p><a href="#">Home</a> / FAQ</p>
        <h2>Frequently Asked Questions</h2>
      </div>
      <div class="col-md-12">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> What is BestValue.com? </a> </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <p>Donec hendrerit accumsan urna, eget luctus justo malesuada at. Proin non venenatis eros, sed aliquet metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas metus metus, varius quis mi non, vestibulum cursus augue. Aliquam quis vestibulum nibh. Morbi laoreet elit ut tincidunt eleifend. Nulla facilisi. Donec gravida erat quis odio accumsan mollis. </p>
                <p><span>Vestibulum vitae ante vel lectus pulvinar euismod consectetur id velit. Morbi euismod, risus eu auctor scelerisque, tortor tortor interdum leo, a fringilla sapien magna a odio. Maecenas lacinia tempus tellus, in congue mi fringilla vestibulum. </span></p>
                <p>Mauris purus odio, pharetra et urna at, porta bibendum magna. Cras ligula augue, varius quis efficitur eget, lacinia at lectus. Ut ullamcorper metus eget nisl malesuada, a varius libero porta. Aenean condimentum pellentesque lacus, ac molestie mi iaculis ac. Vivamus tortor nibh, vestibulum sit amet ultrices vel, tempus id nunc. Etiam dignissim ex ut magna faucibus feugiat. Sed venenatis enim vestibulum augue mattis fringilla nec in diam. Mauris id augue tellus. Fusce mi nibh, pulvinar finibus ex quis, accumsan tincidunt felis. Nullam feugiat enim at elit condimentum, at tempus nibh congue. Curabitur ut nibh leo. Nullam efficitur nunc quis purus pretium pulvinar. Proin malesuada felis a tortor maximus, at volutpat purus sagittis. </p>
                <p>Mauris purus odio, pharetra et urna at, porta bibendum magna. Cras ligula augue, varius quis efficitur eget, lacinia at lectus. Ut ullamcorper metus eget nisl malesuada, a varius libero porta. Aenean condimentum pellentesque lacus, ac molestie mi iaculis ac. Vivamus tortor nibh, vestibulum sit amet ultrices vel, tempus id nunc. Etiam dignissim ex ut magna faucibus feugiat. </p>
              </div>
            </div>
          </div>
          <div class="panel panel-primary">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> How can I earn cashback offers on BestValue.com? </a> </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
            </div>
          </div>
          <div class="panel panel-primary">
            <div class="panel-heading" role="tab" id="headingThree">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> From the time I do a transaction, how long does it take to actually get my cashback? </a> </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
              <div class="panel-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
            </div>
          </div>
          <div class="panel panel-primary">
            <div class="panel-heading" role="tab" id="headingFour">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour"> Can I visit the retailer directly, and just send you a receipt to get my cashback? </a> </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
              <div class="panel-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
            </div>
          </div>
          <div class="panel panel-primary">
            <div class="panel-heading" role="tab" id="headingFive">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive"> What are Rewards? </a> </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
              <div class="panel-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
            </div>
          </div>
          <div class="panel panel-primary">
            <div class="panel-heading" role="tab" id="headingSix">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix"> How do you request your Cashback and Rewards together? </a> </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
              <div class="panel-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
            </div>
          </div>
          <div class="panel panel-primary">
            <div class="panel-heading" role="tab" id="headingSeven">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven"> Where does cashback come from? </a> </h4>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
              <div class="panel-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
            </div>
          </div>
          <div class="panel panel-primary">
            <div class="panel-heading" role="tab" id="headingEight">
              <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight"> From the time I do a transaction, how long does it take to actually get my cashback? </a> </h4>
            </div>
            <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
              <div class="panel-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
            </div>
          </div>
        </div>
      </div>
      <div class="shadow"></div>
    </div>
  </div>

<section class="container">
	<div class="newsletter abt">
    	<div class="col-md-5 col-sm-5 col-xs-12">
            <div class="buy">
            <h4>Buy and Earn</h4>
            <h1>Extra Cash Back</h1>
            </div>
            <img src="{{ asset('assets/images/hand.png') }}" />
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 log">
            <p>log on to</p>
            <h4>www.bestvalue.com</h4>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 btmlogo no-padding"><img src="{{ asset('assets/images/btmlogo.png') }}"/></div>
    </div>
</section>
</div>
<script>
			$(function() {
				var demo1 = $("#demo1").slippry({
					// transition: 'fade',
					// useCSS: true,
					// speed: 1000,
					// pause: 3000,
						auto: true,
					// preload: 'visible',
					// autoHover: false
				});

			});
		</script>
@endsection
