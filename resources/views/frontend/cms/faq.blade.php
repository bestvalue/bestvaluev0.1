@extends('frontend.layouts.master')
<style>
/* Remove the navbar's default margin-bottom and rounded borders */
.navbar {
	margin-bottom: 0;
	border-radius: 0;
}
/* Add a gray background color and some padding to the footer */
footer {
	background-color: #f2f2f2;
	padding: 25px;
}
</style>
@section('content')
<div class="container-fluid inner_bg">
  <div class="container">
    <div class="row">
      <div class="col-md-12 space1">
        <p><a href="#">Home</a> / FAQ</p>
        <h2>Frequently Asked Questions</h2>
      </div>
      <div class="col-md-12">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					@if (count($questions))
					@foreach ($questions as $k=>$question)
				  <?php $class=( $k==0 ? "in" : "") ?>
					<div class="panel panel-primary">
            <div class="panel-heading" role="tab" id="heading{{$k}}">
              <h4 class="panel-title"> <a class="collapsed" aria-expanded="true" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$k}}"  aria-controls="collapse{{$k}}"> {{$question->question}} </a> </h4>
            </div>
            <div id="collapse{{$k}}" class="panel-collapse collapse {{ $class }}"  role="tabpanel" aria-labelledby="heading{{$k}}">
              <div class="panel-body">   {{$question->answer}}
							</div>
             </div>
          </div>
					@endforeach
					@else
							{{'NO data'}}
					@endif

          </div>
        </div>
      </div>
      <div class="shadow"></div>
    </div>
  </div>

<section class="container">
	<div class="newsletter abt">
    	<div class="col-md-5 col-sm-5 col-xs-12">
            <div class="buy">
            <h4>Buy and Earn</h4>
            <h1>Extra Cash Back</h1>
            </div>
            <img src="{{ asset('assets/images/hand.png') }}" />
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 log">
            <p>log on to</p>
            <h4>www.bestvalue.com</h4>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 btmlogo no-padding"><img src="{{ asset('assets/images/btmlogo.png') }}"/></div>
    </div>
</section>
</div>
<script>
			$(function() {
				var demo1 = $("#demo1").slippry({
					// transition: 'fade',
					// useCSS: true,
					// speed: 1000,
					// pause: 3000,
						auto: true,
					// preload: 'visible',
					// autoHover: false
				});

			});
		</script>
@endsection
