@extends('frontend.layouts.master')
@section('content')
<section>
    <div class="container-fluid no-padding">
        <div class="abtusbnr">
            <img src="{{ asset('assets/images/abtbnr.jpg') }}"/>
            <div class="container">
                <div class="pagetxt">Home / About Us</div>
                <div class="abttxt">
                    <img src="{{ asset('assets/images/orgpatrn.png') }}"/>
                    <div class="innertxt">
                    <h2>About BestValue</h2>
                    <p> content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. </p>
                </div>
            </div>
          </div>
        </div>
    </div>
</section>
{!! $page->content !!}
<section class="container">
	<div class="newsletter abt">
    	<div class="col-md-5 col-sm-5 col-xs-12">
            <div class="buy">
            <h4>Buy and Earn</h4>
            <h1>Extra Cash Back</h1>
            </div>
            <img src="{{ asset('assets/images/hand.png') }}" />
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 log">
            <p>log on to</p>
            <h4>www.bestvalue.com</h4>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 btmlogo no-padding"><img src="{{ asset('assets/images/btmlogo.png') }}"/></div>
    </div>
</section>
<script>
			$(function() {
				var demo1 = $("#demo1").slippry({
					// transition: 'fade',
					// useCSS: true,
					// speed: 1000,
					// pause: 3000,
						auto: true,
					// preload: 'visible',
					// autoHover: false
				});

			});
		</script>
@endsection
