@extends('frontend.layouts.master')
<style>
  /* Remove the navbar's default margin-bottom and rounded borders */
  .navbar {
    margin-bottom: 0;
    border-radius: 0;
  }

  /* Add a gray background color and some padding to the footer */
  footer {
  background-color: #f2f2f2;
    padding: 25px;
  }
</style>
@section('content')
<div class="jumbotron">
<div class="container">
<div class="col-md-9 col-sm-9 col-xs-12 no-padding slider-div">
<section class="demo_wrapper">
<article class="demo_block">
        <ul id="demo1">
            <li><a href="#slide1"><img src="{{ asset('assets/images/slider/image-1.jpg')}}" alt=""></a></li>
            <li><a href="#slide2"><img src="{{ asset('assets/images/slider/image-2.jpg')}}"  alt=""></a></li>
            <li><a href="#slide3"><img src="{{ asset('assets/images/slider/image-4.jpg')}}" alt=""></a></li>
        </ul>
</article>
</section>
</div>
<div class="col-md-3 col-sm-3 col-xs-12 no-padding how-it-works">
<img src="{{ asset('assets/images/how-it-works.jpg')}}" class="img-responsive" >
</div>
</div>
</div>
<div class="container-fluid content-bg">
<div class="container bg-3">
<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
  <h3 class="col-md-6 col-sm-6  col-xs-12 no-padding offer-hd">Get Best Offers</h3>
  <h6 class="col-md-6 col-sm-6  col-xs-12 text-right no-padding">You will get the best offers here</h6>
 </div>
<div class="row text-center">
        <div class="col-sm-3">
          <div class="list-item-top">
              <img src="{{ asset('assets/images/logo-mytra.jpg')}}">
            </div>
            <div class="list-item-cntnt box-style1">
              <p>Get 45% Flat <br>
Discount on all <br>
<span>Ladies</span> Winter Dress</p>
  <p>Upto <b>20% </b>discounts <br>
on Cometic Products</p>
  <a href="#" class="btn btn-pink">Get More Offers</a>
            </div>
        </div>
        <div class="col-sm-3">
          <div class="list-item-top">
              <img src="{{ asset('assets/images/logo-snapdeal.jpg')}}">
            </div>
            <div class="list-item-cntnt box-style1">
              <p>Summer Sale Offer<br>
            Buy One Get One<br>
            <b>Offer!</b><br>
            All Ladies Apparels<br>
            </p>
  <p>
              <img src="{{ asset('assets/images/buyt-it-icon.png')}}" >
            </p>
  <a href="#" class="btn btn-pink">Get More Offers</a>
            </div>
        </div>
        <div class="col-sm-3">
          <div class="list-item-top">
              <img src="{{ asset('assets/images/logo-amazone.jpg')}}">
            </div>
            <div class="list-item-cntnt box-style1">
              <p>Exciting Offers in all Brands</p>
  <p>
              <img src="{{ asset('assets/images/logo-1.png')}}">
                <img src="{{ asset('assets/images/logo-2.png')}}">
                <img src="{{ asset('assets/images/logo-3.png')}}">
                <img src="{{ asset('assets/images/logo4.png')}}">
                <img src="{{ asset('assets/images/logo-5.png')}}">
            </p>
  <a href="#" class="btn btn-pink">Get More Offers</a>
            </div>
        </div>
        <div class="col-sm-3">
          <div class="list-item-top">
              <img src="{{ asset('assets/images/logo-ebay.jpg')}}">
            </div>
            <div class="list-item-cntnt box-style2">
              <ul>
                  <li>Buy one get one</li>
                    <li>Upto 40% discounts</li>
                    <li>20% Discount on Kurtis</li>
                    <li>15% Flat Discount on</li>
                    <li>Cosmetic Products</li>
                    <li>Buy more than Rs 1500 and get </li>
                    <li>200 Cash Back</li>
  </ul>
  <a href="#" class="btn btn-pink">Get More Offers</a>
            </div>
        </div>
        <div class="col-sm-3">
          <div class="list-item-top">
              <img src="{{ asset('assets/images/logo-flipkart.jpg')}}">
            </div>
            <div class="list-item-cntnt box-style1">
              <p>Exciting Offers in all Brands</p>
  <p>
              <img src="{{ asset('assets/images/logo-1.png')}}">
                <img src="{{ asset('assets/images/logo-2.png')}}">
                <img src="{{ asset('assets/images/logo-3.png')}}">
                <img src="{{ asset('assets/images/logo4.png')}}">
                <img src="{{ asset('assets/images/logo-5.png')}}">
            </p>
  <a href="#" class="btn btn-pink">Get More Offers</a>
            </div>
        </div>
        <div class="col-sm-3">
          <div class="list-item-top">
              <img src="{{ asset('assets/images/logo-paytm.jpg')}}">
            </div>
            <div class="list-item-cntnt box-style2">
              <ul>
                  <li>Buy one get one</li>
                    <li>Upto 40% discounts</li>
                    <li>20% Discount on Kurtis</li>
                    <li>15% Flat Discount on</li>
                    <li>Cosmetic Products</li>
                    <li>Buy more than Rs 1500 and get </li>
                    <li>200 Cash Back</li>
  </ul>
  <a href="#" class="btn btn-pink">Get More Offers</a>
            </div>
        </div>
        <div class="col-sm-3">
          <div class="list-item-top">
              <img src="{{ asset('assets/images/logo-shopnix.jpg')}}">
            </div>
            <div class="list-item-cntnt box-style1">
              <p>Get 45% Flat <br>
Discount on all <br>
<span>Ladies</span> Winter Dress</p>
  <p>Upto <b>20% </b>discounts <br>
on Cometic Products</p>
  <a href="#" class="btn btn-pink">Get More Offers</a>
            </div>
        </div>
        <div class="col-sm-3">
          <div class="list-item-top">
              <img src="{{ asset('assets/images/logo-jabong.jpg')}}">
            </div>
            <div class="list-item-cntnt box-style1">
              <p>Summer Sale Offer<br>
            Buy One Get One<br>
            <b>Offer!</b><br>
            All Ladies Apparels<br>
            </p>
  <p>
              <img src="{{ asset('assets/images/buyt-it-icon.png')}}" >
            </p>
  <a href="#" class="btn btn-pink">Get More Offers</a>
            </div>
        </div>
</div>
</div>
</div>
<section class="container-fluid bottom-section">
<div class="container">
<div class="col-md-7">
  <h3>Start Shopping</h3>
    <div class="col-md-12 bottom-steps">
      <span>1</span>
        <div class="bttm-step">
          <h5>Create Account</h5>
          <font>Ut quis nisl non justo pulvinar consectetur. Nam interdum nunc sapien, ac fermentum lectus condimentum eu. Vestibulum interdum, massa vitae dapibus aliquam Proin ullamcorper dolor et nisl porttitor varius. </font>
      </div>
    </div>
    <div class="col-md-12 bottom-steps">
      <span>2</span>
        <div class="bttm-step">
          <h5>Choose Offers</h5>
          <font>Ut quis nisl non justo pulvinar consectetur. Nam interdum nunc sapien, ac fermentum lectus condimentum eu. Vestibulum interdum, massa vitae dapibus aliquam Proin ullamcorper dolor et nisl porttitor varius. </font>
      </div>
    </div>
    <div class="col-md-12 bottom-steps">
      <span>3</span>
        <div class="bttm-step">
          <h5>Buy @ Best Price</h5>
          <font>Ut quis nisl non justo pulvinar consectetur. Nam interdum nunc sapien, ac fermentum lectus condimentum eu. Vestibulum interdum, massa vitae dapibus aliquam Proin ullamcorper dolor et nisl porttitor varius. </font>
      </div>
    </div>
</div>
<div class="model-girl1">
  <img src="{{ asset('assets/images/model-girl1.png')}}">
</div>
</div>
</section>
<section class="container">
<div class="newsletter">
<span class="col-md-6 newsletter-txt">Please Subscribe our Newsletter,
You will get notification, latest offers
and promotions.</span>
<div class="col-md-6">
  <h5 class="newslettr-hd">Subscribe Our Newsletter</h5>
  <div class="input-group newslttr-form">
      <input type="text" class="form-control" placeholder="Enter Email Address">
<span class="input-group-btn">
  <button class="btn btn-default" type="button">Subscribe</button>
        </span>
    </div><!-- /input-group -->
</div>
</div>
</section>

<!-- Login Pop Up -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
<div class="modal-dialog">
<div class="loginmodal-container">
  <h1>Login to Your Account <img src="{{ asset('assets/images/logo-login.png')}}" /></h1><br>
  <form>
  <input type="text" name="user" placeholder="Username">
  <input type="password" name="pass" placeholder="Password">
  <input type="submit" name="login" class="login loginmodal-submit" value="Login">
  </form>

  <div class="login-help">
  <a href="#" >Register</a>  <a href="#">Forgot Password</a>
  </div>
</div>
</div>
</div>
<!-- Login Pop Up -->

<!-- Register Pop Up -->
<div class="modal fade" id="login-modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
<div class="modal-dialog">
<div class="loginmodal-container">
<div class="col-md-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <form role="form">
                    <h2>Sign Up</h2>
                    <hr class="colorgraph">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="first_name" id="first_name" class="form-control input-lg" placeholder="First Name" tabindex="1">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="last_name" id="last_name" class="form-control input-lg" placeholder="Last Name" tabindex="2">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="email" name="display_name" id="display_name" class="form-control input-lg" placeholder="Email Address" tabindex="3">
                    </div>
                    <div class="form-group">
                        <input type="text" name="email" id="email" class="form-control input-lg" placeholder="Phone" tabindex="4">
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" tabindex="5">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-lg" placeholder="Confirm Password" tabindex="6">
                            </div>
                        </div>
                    </div>


                    <hr class="colorgraph">
                    <div class="row">
                        <div class="col-xs-6 col-md-6"><input class="register-btn" type="submit" value="Register" ctabindex="7"></div>
                        <div class="col-xs-6 col-md-6"><input class="login-btn" type="submit" value="Sign In"tabindex="7"></div>
                    </div>
                </form>
            </div>
        </div>
</div>
</div>
</div>
</div>
<!-- Login Pop Up -->

<script>
$(function() {
var demo1 = $("#demo1").slippry({
  // transition: 'fade',
  // useCSS: true,
  // speed: 1000,
  // pause: 3000,
    auto: true,
  // preload: 'visible',
  // autoHover: false
});

});
</script>

</div>
@endsection
