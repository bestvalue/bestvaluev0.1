<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{{ csrf_token() }}" />

        <title>@yield('page-title') | {{ settings('app_name') }}</title>

        <!-- Meta -->
        <meta name="description" content="@yield('meta_description', 'Default Description')">
        <meta name="author" content="@yield('meta_author', 'John Wick 2')">
        @yield('meta')

        <!-- Styles -->
        @yield('before-styles-end')
        {!! Html::style(elixir('css/frontend.css')) !!}
        @yield('after-styles-end')
        <!-- gkr update -->
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}"type="text/css" >
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}"type="text/css" >
        <link rel="stylesheet" href="{{ asset('assets/css/slippry.css') }}"type="text/css" >
        <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }} "type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/css/jquery.bxslider.css') }}"type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/css/innerpage.css') }}"type="text/css">

        <!-- JavaScripts -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

        <script src="{{ asset('assets/js/bootstrap.js') }}"></script>
        <script src="{{ asset('assets/js/slippry.min.js') }}"></script>
        <script src="//use.edgefonts.net/cabin;source-sans-pro:n2,i2,n3,n4,n6,n7,n9.js"></script>
        <script src="{{ asset('assets/js/jquery.bxslider.js') }}"></script>

        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

        <style>
        /* Remove the navbar's default margin-bottom and rounded borders */
        .navbar {
        	margin-bottom: 0;
        	border-radius: 0;
        }
        /* Add a gray background color and some padding to the footer */
        footer {
        	background-color: #f2f2f2;
        	padding: 25px;
        }
        </style>

    </head>
    <body id="app-layout">

        @include('frontend.includes.nav')
          <!-- container -->
        @yield('content')
          <!-- container -->
        @include('frontend.includes.footer')



        <!-- @yield('before-scripts-end')
        {!! Html::script(elixir('js/frontend.js')) !!}
        @yield('after-scripts-end') -->
    </body>
</html>
