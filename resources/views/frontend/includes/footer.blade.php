<section class="container text-center footer-logo">
<a href="#">
<img src="{{ asset('assets/images/f-logo1.png')}}">
</a>
<a href="#">
<img src="{{ asset('assets/images/f-logo2.png')}}">
</a>
<a href="#">
<img src="{{ asset('assets/images/f-logo3.png')}}">
</a>
<a href="#">
<img src="{{ asset('assets/images/f-logo4.png')}}">
</a>
<a href="#">
<img src="{{ asset('assets/images/f-logo5.png')}}">
</a><a href="#">
<img src="{{ asset('assets/images/f-logo6.png')}}">
</a>
<a href="#">
<img src="{{ asset('assets/images/f-logo7.png')}}">
</a>
<a href="#">
<img src="{{ asset('assets/images/f-logo1.png')}}">
</a>
</section>
<footer class="container-fluid">
<div class="container no-padding">
<div class="col-md-2 col-sm-4 col-xs-6">
    <ul>
        <li><a href="#">Snapdeal</a></li>
        <li><a href="#">Myntra</a></li>
        <li><a href="#">Goibibo Hotels</a></li>
        <li><a href="#">ShopClues</a></li>
        <li><a href="#">Lenskart</a></li>
        <li><a href="#">Infibeam</a></li>
        <li><a href="#">Cleartrip</a></li>
        <li><a href="#">GoDaddy</a></li>
        <li><a href="#">Printvenue</a></li>
    </ul>
</div>
<div class="col-md-2 col-sm-4 col-xs-6">
    <ul>
        <li><a href="#">Snapdeal</a></li>
        <li><a href="#">Myntra</a></li>
        <li><a href="#">Goibibo Hotels</a></li>
        <li><a href="#">ShopClues</a></li>
        <li><a href="#">Lenskart</a></li>
        <li><a href="#">Infibeam</a></li>
        <li><a href="#">Cleartrip</a></li>
        <li><a href="#">GoDaddy</a></li>
        <li><a href="#">Printvenue</a></li>
    </ul>
</div>
<div class="col-md-2 col-sm-4 col-xs-6">
    <ul>
        <li><a href="#">Snapdeal</a></li>
        <li><a href="#">Myntra</a></li>
        <li><a href="#">Goibibo Hotels</a></li>
        <li><a href="#">ShopClues</a></li>
        <li><a href="#">Lenskart</a></li>
        <li><a href="#">Infibeam</a></li>
        <li><a href="#">Cleartrip</a></li>
        <li><a href="#">GoDaddy</a></li>
        <li><a href="#">Printvenue</a></li>
    </ul>
</div>
<div class="col-md-2 col-sm-4 col-xs-6">
    <ul>
        <li><a href="#">Snapdeal</a></li>
        <li><a href="#">Myntra</a></li>
        <li><a href="#">Goibibo Hotels</a></li>
        <li><a href="#">ShopClues</a></li>
        <li><a href="#">Lenskart</a></li>
        <li><a href="#">Infibeam</a></li>
        <li><a href="#">Cleartrip</a></li>
        <li><a href="#">GoDaddy</a></li>
        <li><a href="#">Printvenue</a></li>
    </ul>
</div>
<div class="col-md-2 col-sm-4 col-xs-6">
    <ul>
        <li><a href="#">Snapdeal</a></li>
        <li><a href="#">Myntra</a></li>
        <li><a href="#">Goibibo Hotels</a></li>
        <li><a href="#">ShopClues</a></li>
        <li><a href="#">Lenskart</a></li>
        <li><a href="#">Infibeam</a></li>
        <li><a href="#">Cleartrip</a></li>
        <li><a href="#">GoDaddy</a></li>
        <li><a href="#">Printvenue</a></li>
    </ul>
</div>
<div class="col-md-2 col-sm-4 col-xs-6">
    <ul>
        <li><a href="#">Snapdeal</a></li>
        <li><a href="#">Myntra</a></li>
        <li><a href="#">Indiatimes Shopping</a></li>
        <li><a href="#">ShopClues</a></li>
        <li><a href="#">Lenskart</a></li>
        <li><a href="#">Infibeam</a></li>
        <li><a href="#">Cleartrip</a></li>
        <li><a href="#">GoDaddy</a></li>
        <li><a href="#">Printvenue</a></li>
    </ul>
</div>
<div class="col-md-12">
    <ul class="list-inline link-footer">
        <li>{!! link_to_route('frontend.about', 'About us') !!}</li>
        <li>{!! link_to_route('frontend.faq', 'FAQs') !!}</li>
        <li><a href="#">Sitemap  </a></li>
        <li>{!! link_to_route('frontend.contact', 'Contact us') !!}</li>
        <li>{!! link_to_route('frontend.terms', 'Terms & Conditions') !!}</a></li>
        <li>{!! link_to_route('frontend.partner', 'Partner with us') !!}</a></li>
        <li>{!! link_to_route('frontend.privacyPolicy', 'Privacy & Cookie Policy') !!}</a></li>
        <li>{!! link_to_route('frontend.blog', 'Blog') !!}</a></li>
        <li>{!! link_to_route('frontend.careers', 'Careers') !!}</a></li>

    </ul>
    <div class="social-media pull-right">
        <a href="#">
            <img src="{{ asset('assets/images/footer-fb.png')}}">
        </a>
        <a href="#">
            <img src="{{ asset('assets/images/footer-lin.png')}}">
        </a>
        <a href="#">
            <img src="{{ asset('assets/images/footer-tw.png')}}">
        </a>
</div>
</div>
</div>
</footer>
<div class="footer-bottom text-center">All Copyrights are Reserved by<a href="#"> BestValue.com</a></div>
