<div class="container">
	<div class="col-md-6 no-padding">
    	<span class="logo">
        	<a href="#"><img src="{{ asset('assets/images/logo.png') }}" /></a>
        </span>
    </div>
    <div class="col-md-6 no-padding header-right">
    	<div class="col-md-12 top-right-menu no-padding">
        	<!-- <a class="btn btn-sm btn-default pull-right"  data-toggle="modal" data-target="#login-modal2">REGISTER</a>
            <a class="btn btn-sm btn-default pull-right" data-toggle="modal" data-target="#login-modal">SIGN IN</a> -->
        		<!-- Authentication Links -->
						@if (Auth::guest())
						   {{ HTML::link('register', 'REGISTER', array('class' => 'btn btn-sm btn-default pull-right'))}}
							 {{ HTML::link('login', 'SIGN IN', array('class' => 'btn btn-sm btn-default pull-right'))}}
						@else
										<a href="#" class="btn btn-sm btn-default pull-right dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
												{{ Auth::user()->present()->name }} <span class="caret"></span>
										</a>

										<ul class="dropdown-menu pull-right" role="menu">
												<li>{!! link_to_route('dashboard', trans('app.dashboard')) !!}</li>

												<li>{!! link_to_route('auth.logout', trans('app.logout')) !!}</li>
										</ul>
						@endif
            <ul class="list-inline pull-right menu-list-top">
            	<li><a href="#">SUPPORT 24x7</a></li>
            	<li><a href="#">HELP</a></li>
            </ul>
        </div>
     	<div class="col-md-12  col-sm-12 col-xs-12 no-padding search-div">
            <div class="input-group">
            	<input type="text" class="form-control" placeholder="Anything in mind?">
				<span class="input-group-btn">
					<button class="btn btn-default search-btn" type="button">SEARCH</button>
              	</span>
            </div><!-- /input-group -->
        </div>

    </div>
</div>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
  	<div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse no-padding" id="myNavbar">
      <ul class="nav navbar-nav">
        <li>{!! link_to_route('frontend.index', 'Home') !!}</li>
        <li><a href="#">All Stores</a></li>
        <li><a href="#">Deals by Product</a></li>
        <li><a href="#">Hot Deals</a></li>
        <li><a href="#">How it works</a></li>
      </ul>
      <div class="my-cart">
      	My Cart (<span>0</span>)
      </div>
    </div>
    </div>
  </div>
</nav>
