<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Http\Request;
use Illuminate\Support\Facades\Input as Input ;
use App\Cms as CmsModel;
use App\Faq as FaqModel;
use App\Blogs as BlogModel;
use App\Job_opening as JobModel;

class PageController extends Controller {

	public function __construct() {

		$CmsModel = new CmsModel;
	}


	public function faq() {
		$questions=FaqModel::all();
		return view('frontend.cms.faq',compact('questions'));
	}
	public function privacyPolicy() {
		$page=CmsModel::find(2);
		return view('frontend.cms.privacy-policy',compact('page'));
	}
	public function partner() {
		return view('frontend.cms.partner-with-us');
	}
	public function about() {
		$page=CmsModel::find(1);
		return view('frontend.cms.about-us',compact('page'));
	}
	public function terms() {
		$page=CmsModel::find(3);
		return view('frontend.cms.terms',compact('page'));
	}
	public function blog() {
		$blogs=BlogModel::All();
		return view('frontend.cms.blogs',compact('blogs'));
	}
	public function blogDetail($id) {
		$blog=BlogModel::find($id);
		return view('frontend.cms.blog-detail',compact('blog'));
	}
	public function careers() {
		$page=CmsModel::find(4);
		$jobs=JobModel::All();
		return view('frontend.cms.careers',compact('page','jobs'));
	}
	public function contact() {
		return view('frontend.cms.contact-us');
	}
	public function contactPost() {
		?><script>alert("Thank you for contacting us")</script> <?php

		// echo $name = Input::get('name', 'Sally');
		// die;
	  return view('frontend.index');
	}
}
