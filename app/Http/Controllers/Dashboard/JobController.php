<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Job_opening as JobModel;
use Illuminate\Support\Facades\Input as Input ;

/**
 * Class JobController
 * @package App\Http\Controllers
 */
class JobController extends Controller {


	/**
	 * Displays the page with all available Job Openings
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index() {
		$openings=JobModel::all();
		return view('dashboard.cms.job.index', compact('openings'));
	}

	/**
	 * Displays the form for creating new Opening.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create() {
		$edit = false;

		return view('dashboard.cms.job.add-edit', compact('edit'));
	}

	/**
	 * Store Job-Opening to database.
	 *
	 * @return mixed
	 */
	public function store() {
		$input = Input::all();
		$table = new JobModel;
		$table->title  = $input['title'];
		$table->description = $input['description'];
		$table->save();
		return redirect()->route('dashboard.jobs.index')
			->withSuccess('New Question added Successfully');
	}

	/**
	 * Displays the form for editing specific job opening.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit( $id) {
		$edit = true;
		$opening=JobModel::find($id);
		return view('dashboard.cms.job.add-edit', compact('edit', 'opening'));
	}

	/**
	 * Update specified Job Opening.
	 *
	 * @return mixed
	 */
	public function update($id) {
		$input = Input::all();

		$table = JobModel::findOrFail($id);
		$table->title = $input['title'];
		$table->description = $input['description'];
		$table->save();
		return redirect()->route('dashboard.jobs.index')
			->withSuccess('Question and answers Updated Successfully');
	}

	/**
	 * Destroy the Job Openings.
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function destroy($id) {

		JobModel::destroy($id);

		return redirect()->route('dashboard.jobs.index')
			->withSuccess('Question deleted successfully');
	}

}
