<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Faq as FaqModel;
use Illuminate\Support\Facades\Input as Input ;

/**
 * Class FaqController
 * @package App\Http\Controllers
 */
class FaqController extends Controller {


	/**
	 * Displays the page with all available Faq's.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index() {
		$questions=FaqModel::all();
		// print_r($questions);
		// die;
		return view('dashboard.cms.faq.index', compact('questions'));
	}

	/**
	 * Displays the form for creating new Question.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create() {
		$edit = false;

		return view('dashboard.cms.faq.add-edit', compact('edit'));
	}

	/**
	 * Store questions and answers  to database.
	 *
	 * @return mixed
	 */
	public function store() {
		$input = Input::all();
		$table = new FaqModel;
		$table->question = $input['question'];
		$table->answer = $input['answer'];
		$table->save();
		return redirect()->route('dashboard.faq.index')
			->withSuccess('New Question added Successfully');
	}

	/**
	 * Displays the form for editing specific Question and answers.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit( $question_id) {
		$edit = true;
		$question=FaqModel::find($question_id);
		return view('dashboard.cms.faq.add-edit', compact('edit', 'question'));
	}

	/**
	 * Update specified Question and answers.
	 *
	 * @return mixed
	 */
	public function update($id) {
		$input = Input::all();

		$table = FaqModel::findOrFail($id);
		$table->question = $input['question'];
		$table->answer = $input['answer'];
		$table->save();
		return redirect()->route('dashboard.faq.index')
			->withSuccess('Question and answers Updated Successfully');
	}

	/**
	 * Destroy the Question and answers.
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function destroy($id) {

		FaqModel::destroy($id);

		return redirect()->route('dashboard.faq.index')
			->withSuccess('Question deleted successfully');
	}

}
