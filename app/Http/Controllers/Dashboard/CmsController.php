<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Cms as CmsModel;
use Illuminate\Support\Facades\Input as Input ;

/**
 * Class PermissionsController
 * @package App\Http\Controllers
 */
class CmsController extends Controller {
	/*
	 * CmsController constructor.
	 * @param RoleRepository $roles
	 * @param PermissionRepository $permissions
	 */
	public function __construct() {
		$this->middleware('permission:permissions.manage');
		$CmsModel = new CmsModel;
	}

	/**
	 * Displays  all CMS pages .
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index() {
	  $pages=CmsModel::all();

		return view('dashboard.cms.index',compact('pages'));
	}

	/**
	 * Displays the form for creating new pages.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create() {
		$edit = false;

		return view('dashboard.cms.add-edit', compact('edit'));
	}

	/**
	 * Store page to database. Not useing
	 *
		 */
	public function store(CreatePermissionRequest $request) {
		$this->permissions->create($request->all());

		return redirect()->route('dashboard.permission.index')
			->withSuccess(trans('app.permission_created_successfully'));
	}

	/**
	 * Displays the form for editing specific page.
	 *
	 */
	public function edit($pageId) {
		$edit = true;
		$page=CmsModel::find($pageId);
		return view('dashboard.cms.edit', compact('edit','page'));
	}

	/**
	 * Update specified page.
	 *
	 */
	public function update($id) {

		$input = Input::all();

		$page = CmsModel::findOrFail($id);
		$page->content = $input['description'];
		$page->save();

		return redirect()->route('dashboard.cms.index')
			->withSuccess("Updated");
	}

	/**
	 * Displays the form for editing FAQ page.
	 *
	 */
	public function FaqEdit() {
		$edit=true;
		$questions=FaqModel::all();
		// print_r($page);
		// die;
		return view('dashboard.cms.faq-edit', compact('edit','questions'));
	}

	/**
	 * Destroy the permission if it is removable.
	 *
	 * @param Permission $permission
	 * @return mixed
	 * @throws \Exception
	 */
	public function destroy(Permission $permission) {
		if (!$permission->removable) {
			throw new NotFoundHttpException;
		}

		$this->permissions->delete($permission->id);

		return redirect()->route('dashboard.permission.index')
			->withSuccess(trans('app.permission_deleted_successfully'));
	}

	/**
	 * Update permissions for each role.
	 *
	 * @param Request $request
	 * @return mixed
	 */
	public function saveRolePermissions(Request $request) {
		$roles = $request->get('roles');

		$allRoles = $this->roles->lists('id');

		foreach ($allRoles as $roleId) {
			$permissions = array_get($roles, $roleId, []);
			$this->roles->updatePermissions($roleId, $permissions);
		}

		event(new PermissionsUpdated);

		return redirect()->route('dashboard.permission.index')
			->withSuccess(trans('app.permissions_saved_successfully'));
	}
}
