<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Blogs as BlogModel;
use Illuminate\Support\Facades\Input as Input ;
use Illuminate\Support\Facades\Storage as Storage ;
use Illuminate\Support\Facades\File as File ;
use App\User;
use Auth;
use App\Repositories\User\UserRepository;
use Illuminate\Validation\Validator as Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * Class BlogController
 * @package App\Http\Controllers
 */
class BlogController extends Controller {


	/**
	 * UsersController constructor.
	 * @param UserRepository $users
	 */
	public function __construct(UserRepository $users) {
		$this->middleware('auth');
		$this->middleware('session.database', ['only' => ['sessions', 'invalidateSession']]);
		$this->middleware('permission:users.manage');
		$this->users = $users;
	}

	/**
	 * Displays the page with all available Blogs
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index() {
		$blogs=BlogModel::all();

		return view('dashboard.cms.blog.index', compact('blogs'));
	}

	/**
	 * Displays the form for creating new Opening.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create() {
		$edit = false;

		return view('dashboard.cms.blog.add-edit', compact('edit'));
	}

	/**
	 * Store Blog to database and image file to upload folder
	 *
	 * @return mixed
	 */
	public function store() {
		$input = Input::all();
		$table = new BlogModel;
	  if (Input::hasFile('image'))
		 {
			 $destinationPath="upload/blogs";	 // upload path
			 $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
       $fileName = rand(11111,99999).'.'.$extension; // renameing image
       Input::file('image')->move($destinationPath, $fileName);
			 $table->image=$fileName;
		 }
		$table->title  = $input['title'];
		$table->contents = $input['Content'];
		$table->save();
		return redirect()->route('dashboard.blogs.index')
			->withSuccess('New Blog added Successfully');
	}

	/**
	 * Displays the form for editing specific Blog.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit( $id) {
		$edit = true;
		$blog=BlogModel::find($id);
		return view('dashboard.cms.blog.add-edit', compact('edit', 'blog'));
	}

	/**
	 * Update specified Blog.
	 *
	 * @return mixed
	 */
	public function update($id) {
		$input = Input::all();
		$table = BlogModel::findOrFail($id);
		// echo $table->image; die;
		if (Input::hasFile('image'))
		 {
			 File::delete('upload/blogs/'.$table->image); //delete current file
			//  Storage::delete('upload/blogs/123.jpg'); //delete current file

			 $destinationPath="upload/blogs";	 // upload path
			 $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
       $fileName = rand(11111,99999).'.'.$extension; // renameing image
       Input::file('image')->move($destinationPath, $fileName);
			 $table->image=$fileName;
		 }
		$table->title = $input['title'];
		$table->contents = $input['Content'];
		$table->save();

		// File::delete($filename);
		return redirect()->route('dashboard.blogs.index')
			->withSuccess('Blog Updated Successfully');
	}

	/**
	 * Destroy the Blog.
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function destroy($id) {
		$table = BlogModel::findOrFail($id);

		File::delete('upload/blogs/'.$table->image);
		BlogModel::destroy($id);

		return redirect()->route('dashboard.blogs.index')
			->withSuccess('Blog deleted successfully');
	}

}
